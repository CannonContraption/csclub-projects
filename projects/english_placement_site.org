* English Placement Site

** General Idea
   The English department has reached out to us with a request for a new writing
   placement test site. This should be something tied to student IDs where the
   students' information can be tied to an answer without letting the graders know
   which student goes with which response.
   
** Suggested First Steps
   These steps should be done before coding begins, simply to avoid pitfalls
   involved with disparate components not talking to one another.
*** TODO Design interface
    This involves coming up with an interface for instructors and students.
    Students need a way of authenticating themselves, ensuring that they are
    indeed who they say they are, and the instructors need an interface that shows
    student responses in a simple view without exposing identities. This may
    involve a list of responses and some batch commands, or individual panes per
    response, or something entirely different. The interface should allow for
    batch processing in some form or another, simply because there will likely be
    a large volume of responses.
*** TODO Design authentication backend
    There needs to be a somewhat-secure method for allowing students to log in to
    the system and type their response, without allowing them to do someone else's
    work, or viewing anyone else's work. There also needs to be a way to identify
    instructors in order to hand them the correct interface.
*** TODO Design storage backend
    Another piece of the puzzle is a way to store the responses from the students.
    Again, this part needs to be somewhat secure, and it also needs to tie
    responses to students without a character limit on said responses. It should
    at least be capable of handing a list of responses tied in a system similar to
    a tuple so as not to mix responses with each other.
