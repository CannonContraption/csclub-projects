** Raspi Car

** General Idea
   The Raspberry Pi Car is a project students with an interest in robotics or
   hardware can do. The basic idea is you take a set of parts (acquireable from
   a variety of sources, such as Erector, Lego, etc.) and wire them into a
   raspberry pi. This opens up the option for competitions, given a large enough
   group of interested students (basically 2 or more, but it's more fun with more
   people)
