* Rotated Array
** What is a rotated array?
   A rotated array is basically an array where the start and end points have been
   moved. For example, if we have the array {1, 2, 3, 4}, then a rotated version
   could be {3, 4, 1, 2} or {4, 1, 2, 3} or {2, 3, 4, 1}.
