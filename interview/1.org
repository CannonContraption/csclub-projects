* String Permutations
** What is a permutation?
   Permutations of strings are simply different arrangments of letters in a
   string.
** Examples
   | String 1 | String 2 | Are they permutations? |
   |----------+----------+------------------------|
   | hello    | olehl    | yes                    |
   | huiil    | huuil    | no                     |
   | please   | esaelp   | yes                    |
   | play     | lapy     | yes                    |
   | coffee   | tea      | no                     |
   | green    | nreg     | no                     |
