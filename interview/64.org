* Pythagorean Triplets
** What are they?
   Pythagorean triplets (also known as Pythagorean Triples) are sets of three
   whole numbers such that a^2 + b^2 = c^2. The best-known examples are a=3,
   b=4, c=5. 3^2 + 4^2 = 5^2, therefore they follow this pattern.
